CREATE DATABASE book;
ALTER ROLE postgres WITH PASSWORD 'postgres';

DROP TABLE IF EXISTS book;

CREATE TABLE book
(
    id     bigserial NOT NULL
        CONSTRAINT book_pkey
            PRIMARY KEY,
    author VARCHAR(255),
    title  VARCHAR(255)
);

ALTER TABLE book
    owner TO postgres;
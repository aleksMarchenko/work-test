package com.example.demo.services;

import com.example.demo.model.Book;

import java.util.List;

public interface BookService {

    Book create(String author, String title);
    List<Book> getAll();
    Book findByAuthor(String name);
    void deleteByAuthor(String name);
}

package com.example.demo.services;

import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Book create(String author, String title) {
        Book book = new Book(author, title);
        bookRepository.save(book);
        return book;
    }

    @Override
    public List<Book> getAll() {
        return new ArrayList<>(bookRepository.findAll());
    }

    @Override
    public Book findByAuthor(String name) {
        Book book = bookRepository.findByAuthor(name);
        return book;
    }

    @Override
    public void deleteByAuthor(String name) {
        Book byAuthor = bookRepository.findByAuthor(name);
        bookRepository.delete(byAuthor);
    }
}

package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.services.BookService;
import com.example.demo.services.RabbitMQSender;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {

    private final BookService bookService;
    private final RabbitMQSender rabbitMQSender;

    public BookController(BookService bookService, RabbitMQSender rabbitMQSender) {
        this.bookService = bookService;
        this.rabbitMQSender = rabbitMQSender;
    }

    @GetMapping(path = "/hello")
    public String hello() {
        return "Hello";
    }

    @GetMapping(path = "/show")
    public List<Book> getAll() {
        return bookService.getAll();
    }

    @PostMapping("/create")
    public ResponseEntity<Book> createBook(@RequestParam("author") String author,
                                           @RequestParam("title") String title) {
        Book book = bookService.create(author, title);
        rabbitMQSender.send("Book created with: " + " author = " + author + " and title = " + title);
        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    @DeleteMapping(path = "/deleteByAuthor")
    public String deleteBookByAuthor(@RequestParam("author") String author) {
        try {
            bookService.deleteByAuthor(author);
        } catch (Exception e) {
            return e.getMessage();
        }
        return "Deleted";
    }
}
